/*
 *   LW_fnc_borderGuardExecution
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardCountExecution
 *   - LW_var_borderGuardSystemMessages
 *
 *   Working Ammo:
 *   - M_Mo_82mm_AT_LG
 *   - M_Mo_120mm_AT
 *   - M_Mo_120mm_AT_LG
 *   - M_RPG32_AA_F
 *   - R_60mm_HE
 *
 */

if (alive player) then
{
  private ["_position", "_vehicle"];

  _position = getPos player;

  _vehicle = "M_Mo_120mm_AT_LG" createVehicle [_position select 0, _position select 1, 0];

  sleep 0.1;

  if (alive player) then
  {
    player setDamage 1;
  };

  LW_var_borderGuardCountExecution = LW_var_borderGuardCountExecution + 1;

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: You were executed";
  };
};

