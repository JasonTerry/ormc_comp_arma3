/*
 *   LW_fnc_borderGuardMarkers
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 */

private ["_index", "_markerName", "_markerPosition", "_result"];

_result = [];

_markerPosition = [];
_index = 0;

while { !([_markerPosition, [0, 0, 0]] call BIS_fnc_areEqual) } do
{
  _markerName = "LW_BorderGuardMarker";

  if (_index != 0) then
  {
    _markerName = _markerName + "_" + str _index;
  };

  _markerPosition = getMarkerPos _markerName;

  if !([_markerPosition, [0, 0, 0]] call BIS_fnc_areEqual) then
  {
    _result set [count _result, _markerName];
  }
  else
  {
    if (_index == 0) then
    {
      _markerPosition = [];
    };
  };

  _index = _index + 1;
};

if (count _result == 0) then
{
  if (isServer && !isDedicated) then
  {
    [] spawn
    {
      private "_null";
      waitUntil { sleep 0.1; alive player };
      sleep 1;
      _null = "BorderGuard markers not found. You have to place at least one marker called 'LW_BorderGuardMarker' in middle of desired combat zone." call BIS_fnc_errorMsg;
    };
  };
};

_result;