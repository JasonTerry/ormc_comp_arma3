/*
 *   LW_fnc_borderGuardBorders
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardBorderAlpha
 *   - LW_var_borderGuardBorderBrush
 *   - LW_var_borderGuardBorderColor
 *   - LW_var_borderGuardBorderWidth
 *   - LW_var_borderGuardCreateBorders
 *   - LW_var_borderGuardOuterBorders
 *   - LW_var_borderGuardSimpleBorders
 *
 */

if LW_var_borderGuardCreateBorders then
{
  private "_area";

  if LW_var_borderGuardSimpleBorders then
  {
    if (!isNil "LW_OuterBorder") then
    {
      _area = triggerArea LW_OuterBorder;

      if (_area select 3) then
      {
        private ["_angle", "_distance", "_length", "_marker", "_position"];

        _angle = _area select 2;

        {
          if ((_forEachIndex % 2) == 0) then
          {
            _distance = (_area select 1) + LW_var_borderGuardBorderWidth;
            _length = _area select 0;
          }
          else
          {
            _distance = (_area select 0) + LW_var_borderGuardBorderWidth;
            _length = (_area select 1) + (LW_var_borderGuardBorderWidth * 2);
          };

          _position = [LW_OuterBorder, _distance, _angle + _x] call BIS_fnc_relPos;

          _marker = createMarkerLocal ["LW_BorderGuardBorder_" + str _forEachIndex, _position];
          _marker setMarkerAlphaLocal LW_var_borderGuardBorderAlpha;
          _marker setMarkerBrushLocal LW_var_borderGuardBorderBrush;
          _marker setMarkerColorLocal LW_var_borderGuardBorderColor;
          _marker setMarkerDirLocal _angle + _x;
          _marker setMarkerShapeLocal "RECTANGLE";
          _marker setMarkerSizeLocal [_length, LW_var_borderGuardBorderWidth];
        }
        forEach [0, 90, 180, 270];
      };
    }
    else
    {
      if (isServer && !isDedicated) then
      {
        [] spawn
        {
          private "_null";
          waitUntil { sleep 0.1; alive player };
          sleep 1;
          _null = "Outer border not found. If You wish to create visible borders automatically, You have to name the outer border as 'LW_OuterBorder'." call BIS_fnc_errorMsg;
        };
      };
    };
  }
  else
  {
    if (count LW_var_borderGuardOuterBorders > 0) then
    {
      {
        _area = triggerArea _x;

        if (_area select 3) then
        {
          private "_marker";

          _marker = createMarkerLocal ["LW_BorderGuardBorder_" + str _forEachIndex, _x];
          _marker setMarkerAlphaLocal LW_var_borderGuardBorderAlpha;
          _marker setMarkerBrushLocal LW_var_borderGuardBorderBrush;
          _marker setMarkerColorLocal LW_var_borderGuardBorderColor;
          _marker setMarkerDirLocal (_area select 2);
          _marker setMarkerShapeLocal "RECTANGLE";
          _marker setMarkerSizeLocal [_area select 0, _area select 1];
        };
      }
      forEach LW_var_borderGuardOuterBorders;
    }
    else
    {
      if (isServer && !isDedicated) then
      {
        [] spawn
        {
          private "_null";
          waitUntil { sleep 0.1; alive player };
          sleep 1;
          _null = "Outer borders not found.  If You wish to create visible borders automatically, You have to define list of triggers creating the outer border by array 'LW_var_borderGuardOuterBorders'." call BIS_fnc_errorMsg;
        };
      };
    };
  };
};
