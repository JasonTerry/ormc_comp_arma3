/*
 *   LW_fnc_borderGuardMissionTaskComplete
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardCountExecution
 *   - LW_var_borderGuardCountTask
 *   - LW_var_borderGuardCountViolation
 *   - LW_var_borderGuardSystemMessages
 *
 */

if ("LW_task_borderGuardMissionTask" call BIS_fnc_taskExists) then
{
  private ["_task", "_taskState"];

  if (LW_var_borderGuardCountExecution == 0 && LW_var_borderGuardCountTask == 0 && LW_var_borderGuardCountViolation == 0) then
  {
    _taskState = "SUCCEEDED";

    if LW_var_borderGuardSystemMessages then
    {
      systemChat "[LW] Border Guard: Mission task succeeded";
    };
  }
  else
  {
    _taskState = "FAILED";

    if LW_var_borderGuardSystemMessages then
    {
      systemChat "[LW] Border Guard: Mission task failed";
    };
  };

  _task = ["LW_task_borderGuardMissionTask", player] call BIS_fnc_taskReal;
  _task setTaskState _taskState;

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: Mission task complete";
  };
};
