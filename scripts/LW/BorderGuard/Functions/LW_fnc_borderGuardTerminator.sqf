/*
 *   LW_fnc_borderGuardTerminator
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Script handles:
 *   - LW_scr_borderGuard
 *   - LW_scr_borderGuardWarning
 *
 *   Variables:
 *   - LW_var_borderGuardSystemMessages
 *
 */

if (!scriptDone LW_scr_borderGuard) then
{
  terminate LW_scr_borderGuard;
};

if (!scriptDone LW_scr_borderGuardWarning) then
{
  terminate LW_scr_borderGuardWarning;
};

titleText ["", "PLAIN"];

if LW_var_borderGuardSystemMessages then
{
  systemChat "[LW] Border Guard: Border protection deactivated";
};