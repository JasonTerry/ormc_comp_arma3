/*
 *   LW_fnc_borderGuardWarning
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Parameters:
 *   - LW_par_borderGuardWarning
 *
 */



if _this then
{
  private "_message";

  _message = Localize "STR_LW_BORDERGUARD_MESSAGE";

  while { true } do
  {
    titleText [_message, "PLAIN"];
    sleep 1;
  };
}
else
{
  if (LW_par_borderGuardWarning > 0) then
  {
    hintC Localize "STR_LW_BORDERGUARD_HINT";
  }
  else
  {
    titleText [Localize "STR_LW_BORDERGUARD_MESSAGE", "PLAIN"];
  };
};