/*
 *   LW_fnc_borderGuardTaskRemove
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardMarker
 *   - LW_var_borderGuardSystemMessages
 *   - LW_var_borderGuardTaskMarker
 *   - LW_var_borderGuardTaskName
 * ? - LW_var_currentMissionTask [not required]
 *
 */

if (LW_var_borderGuardTaskName call BIS_fnc_taskExists) then
{
  private "_task";

  _task = [LW_var_borderGuardTaskName, player] call BIS_fnc_taskReal;

  LW_var_borderGuardMarker = nil;
  LW_var_borderGuardTaskName = nil;

  player removeSimpleTask _task;

  deleteMarkerLocal LW_var_borderGuardTaskMarker;

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: Task removed";
  };

  if (!isNil "LW_var_currentMissionTask") then
  {
    player setCurrentTask LW_var_currentMissionTask;
  };
};