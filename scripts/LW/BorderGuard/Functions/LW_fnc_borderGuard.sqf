/*
 *   LW_fnc_borderGuard
 *
 *   Script for Arma 3 by LoonyWarrior*
 *
 *   Functions:
 *   - LW_fnc_borderGuardTask
 *   - LW_fnc_borderGuardTerminator
 *   - LW_fnc_borderGuardWarning
 *
 *   Script handles:
 *   - LW_scr_borderGuardAliveCheck
 *   - LW_scr_borderGuardDistanceCheck
 *   - LW_scr_borderGuardTask
 *   - LW_scr_borderGuardWarning
 *
 *   Variables:
 *   - LW_var_borderGuardCountActivation
 *   - LW_var_borderGuardCountViolation
 *   - LW_var_borderGuardSimpleBorders
 *   - LW_var_borderGuardSystemMessages
 *   - LW_var_borderGuardTaskActive
 *
 *   Parameters:
 *   - LW_par_borderGuard
 *   - LW_par_borderGuardActionDelay
 *   - LW_par_borderGuardMayKill
 *   - LW_par_borderGuardTaskDelay
 *
 *   Triggers:
 *   - LW_InnerBorder
 *
 *   Markers:
 *   - LW_BorderGuardMarker
 *
 */

if (LW_par_borderGuard > -1) then
{
  LW_var_borderGuardCountActivation = LW_var_borderGuardCountActivation + 1;

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: Border protection activated";
  };

  if LW_var_borderGuardSimpleBorders then
  {
    private "_null";

    _null = [] spawn
    {
      private "_null";
      waitUntil { sleep 0.1; !triggerActivated LW_InnerBorder };
      _null = call LW_fnc_borderGuardTerminator;
    };
  };

  if (LW_par_borderGuard < 2) then
  {
    LW_scr_borderGuardWarning = false spawn LW_fnc_borderGuardWarning;
  };

  if (LW_par_borderGuard > 0 && !LW_var_borderGuardTaskActive) then
  {
    if (LW_par_borderGuardTaskDelay > 0) then
    {
      sleep LW_par_borderGuardTaskDelay;
    };

    LW_scr_borderGuardTask = [] spawn LW_fnc_borderGuardTask;
  };

  if (LW_par_borderGuardActionDelay > 0) then
  {
    sleep LW_par_borderGuardActionDelay;

    LW_var_borderGuardCountViolation = LW_var_borderGuardCountViolation + 1;

    if LW_var_borderGuardSystemMessages then
    {
      systemChat "[LW] Border Guard: Border violation detected";
    };

    if (LW_par_borderGuardMayKill > 0) then
    {
      private "_null";

      sleep 0.5;

      if !LW_var_borderGuardTaskActive then
      {
        _null = [] spawn LW_fnc_borderGuardExecution;
      }
      else
      {
        if (!scriptDone LW_scr_borderGuardTask) then
        {
          terminate LW_scr_borderGuardTask;
        };

        if (!scriptDone LW_scr_borderGuardDistanceCheck) then
        {
          terminate LW_scr_borderGuardDistanceCheck;
        };

        if (!scriptDone LW_scr_borderGuardAliveCheck) then
        {
          terminate LW_scr_borderGuardAliveCheck;
        };

        _null = false spawn LW_fnc_borderGuardTaskComplete;
      };
    }
    else
    {
      LW_scr_borderGuardWarning = true spawn LW_fnc_borderGuardWarning;
    };
  };
};
