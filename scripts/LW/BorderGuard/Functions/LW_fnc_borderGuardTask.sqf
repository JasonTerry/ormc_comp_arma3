/*
 *   LW_fnc_borderGuardTask
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Functions:
 *   - LW_fnc_borderGuardCheckDistance
 *   - LW_fnc_borderGuardTaskComplete
 *
 *   Script handles:
 *   - LW_scr_borderGuardAliveCheck
 *   - LW_scr_borderGuardDistanceCheck
 *   - LW_scr_borderGuardTask
 *
 *   Variables:
 *   - LW_var_borderGuardCountTask
 *   - LW_var_borderGuardMarker
 *   - LW_var_borderGuardMarkers
 *   - LW_var_borderGuardSimpleBorders
 *   - LW_var_borderGuardSystemMessages
 *   - LW_var_borderGuardTaskActive
 *   - LW_var_borderGuardTaskMarker
 *   - LW_var_borderGuardTaskName
 *
 *   Parameters:
 *   - LW_par_borderGuardTaskDistance
 *   - LW_par_borderGuardTaskDuration
 *
 */

private ["_null", "_taskDistance", "_taskDirection"];

LW_var_borderGuardTaskActive = true;

LW_var_borderGuardTaskName = "LW_task_borderGuardTask_" + str LW_var_borderGuardCountTask;

if LW_var_borderGuardSimpleBorders then
{
  _taskDistance = (player distance LW_InnerBorder) - LW_par_borderGuardTaskDistance;
  _taskDirection = [player, LW_InnerBorder] call BIS_fnc_dirTo;
}
else
{
  LW_var_borderGuardMarker = [LW_var_borderGuardMarkers, player] call BIS_fnc_nearestPosition;
  _taskDistance = (player distance (getMarkerPos LW_var_borderGuardMarker)) - LW_par_borderGuardTaskDistance;
  _taskDirection = [player, (getMarkerPos LW_var_borderGuardMarker)] call BIS_fnc_dirTo;
};

LW_var_borderGuardTaskMarker = createMarkerLocal ["LW_BorderGuardTaskMarker", [player, LW_par_borderGuardTaskDistance, _taskDirection] call BIS_fnc_relPos];

_null = [player, LW_var_borderGuardTaskName, [Localize "STR_LW_BORDERGUARD_TASK_DESCRIPTION", Localize "STR_LW_BORDERGUARD_TASK_TITLE", Localize "STR_LW_BORDERGUARD_TASK_WAYPOINT"], LW_var_borderGuardTaskMarker, true] call BIS_fnc_taskCreate;

if LW_var_borderGuardSystemMessages then
{
  systemChat "[LW] Border Guard: Task created";
};

LW_var_borderGuardCountTask = LW_var_borderGuardCountTask + 1;

LW_scr_borderGuardDistanceCheck = _taskDistance spawn
{
  private "_null";

  waitUntil { sleep 0.1; _this call LW_fnc_borderGuardCheckDistance };

  if (!scriptDone LW_scr_borderGuardTask) then
  {
    terminate LW_scr_borderGuardTask;
  };

  if (!scriptDone LW_scr_borderGuardAliveCheck) then
  {
    terminate LW_scr_borderGuardAliveCheck;
  };

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: Required distance done";
  };

  _null = true spawn LW_fnc_borderGuardTaskComplete;
};

LW_scr_borderGuardAliveCheck = [] spawn
{
  private "_null";

  waitUntil { sleep 0.1; !alive player };

  if (!scriptDone LW_scr_borderGuardTask) then
  {
    terminate LW_scr_borderGuardTask;
  };

  if (!scriptDone LW_scr_borderGuardDistanceCheck) then
  {
    terminate LW_scr_borderGuardDistanceCheck;
  };

  if LW_var_borderGuardSystemMessages then
  {
    systemChat "[LW] Border Guard: Task canceled";
  };

  _null = [LW_var_borderGuardTaskName, "CANCELED"] call BIS_fnc_taskSetState;

  _null = [] spawn LW_fnc_borderGuardTaskRemove;
};

sleep LW_par_borderGuardTaskDuration;

if (!scriptDone LW_scr_borderGuardDistanceCheck) then
{
  terminate LW_scr_borderGuardDistanceCheck;
};

if (!scriptDone LW_scr_borderGuardAliveCheck) then
{
  terminate LW_scr_borderGuardAliveCheck;
};

if LW_var_borderGuardSystemMessages then
{
  systemChat "[LW] Border Guard: Task timed out";
};

_null = (_taskDistance call LW_fnc_borderGuardCheckDistance) spawn LW_fnc_borderGuardTaskComplete;

