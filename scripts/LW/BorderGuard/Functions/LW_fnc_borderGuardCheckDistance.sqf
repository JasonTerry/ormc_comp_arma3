/*
 *   LW_fnc_borderGuardCheckDistance
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardMarker
 *   - LW_var_borderGuardSimpleBorders
 *
 *
 */

private "_distance";

if LW_var_borderGuardSimpleBorders then
{
  _distance = player distance LW_InnerBorder;
}
else
{
  _distance = player distance (getMarkerPos LW_var_borderGuardMarker);
};

if (_distance < _this) then
{
  true;
}
else
{
  false;
};