/*
 *   LW_fnc_borderGuardMissionTask
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardVersion
 *
 *   Parameters:
 *   - LW_par_borderGuardTaskDistance
 *   - LW_par_borderGuardTaskDuration
 *   - LW_var_borderGuardVersion
 *
 */

private ["_description", "_null", "_version"];

_description = format [Localize "STR_LW_BORDERGUARD_MISSION_TASK_DESCRIPTION", "<br />"];

if (LW_par_borderGuard > 0) then
{
  private "_descriptionTask";
  _descriptionTask = format [Localize "STR_LW_BORDERGUARD_MISSION_TASK_DESCRIPTION_TASK", "<br />", LW_par_borderGuardTaskDistance, LW_par_borderGuardTaskDuration];
  _description = format ["%1<br /><br /><br />%2", _description, _descriptionTask];
};

_version = format ["Borders protected by BorderGuard v%1", LW_var_borderGuardVersion];
_description = format ["%1<br /><br />%2", _description, _version];

_null = [player, "LW_task_borderGuardMissionTask", [_description, Localize "STR_LW_BORDERGUARD_MISSION_TASK_TITLE", ""], objNull, false] call BIS_fnc_taskCreate;
