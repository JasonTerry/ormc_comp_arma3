/*
 *   LW_fnc_borderGuardTask
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   Variables:
 *   - LW_var_borderGuardSystemMessages
 *   - LW_var_borderGuardTaskActive
 *   - LW_var_borderGuardTaskName
 *
 *   Parameters:
 *   - LW_par_borderGuardMayKill
 *
 */

if (LW_var_borderGuardTaskName call BIS_fnc_taskExists) then
{
  private "_null";

  if _this then
  {
    if LW_var_borderGuardSystemMessages then
    {
      systemChat "[LW] Border Guard: Task succeeded";
    };

    _null = [LW_var_borderGuardTaskName, "SUCCEEDED"] call BIS_fnc_taskSetState;
  }
  else
  {
    if LW_var_borderGuardSystemMessages then
    {
      systemChat "[LW] Border Guard: Task failed";
    };

    _null = [LW_var_borderGuardTaskName, "FAILED"] call BIS_fnc_taskSetState;

    if (alive player && LW_par_borderGuardMayKill > 0) then
    {
      _null = [] spawn LW_fnc_borderGuardExecution;
    };
  };

  _null = [] spawn LW_fnc_borderGuardTaskRemove;
};

LW_var_borderGuardTaskActive = false;