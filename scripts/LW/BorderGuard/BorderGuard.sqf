private "_LW_path_functionsBorderGuard";

_LW_path_functionsBorderGuard = "Scripts\LW\BorderGuard\Functions\";

/*
 *   BorderGuard Initialization File
 *
 *   Script for Arma 3 by LoonyWarrior
 *
 *
 *   *** *** *** *** *** *** *** *** *** *** ***
 *
 *   Please notice the private variable above
 *
 *   _LW_path_functionsBorderGuard = 'Scripts\LW\BorderGuard\Functions\';
 *
 *   This represents path to the BorderGuard functions in Your mission folder.
 *
 *   *** *** *** *** *** *** *** *** *** *** ***
 *
 *   Implementation:
 *
 *   You have to decide what type of borders You will use.
 *
 *   Simple Borders - Two triggers surrounding whole combat zone
 *
 *   Multiple Borders - Multiple triggers around the combat zone
 *
 *   = = = = = = = = = = = = = = = = = = = = = =
 *
 *   Simple Borders:
 *   - Create two triggers
 *
 *   - Trigger representing inner border:
 *     - Name: LW_InnerBorder
 *     - Shape: Rectangle
 *     - Type: None
 *     - Activation: Anybody - Repeatedly - Present
 *     - Condition: alive player && !(player in thisList)
 *     - On Activation: LW_scr_borderGuard = [] spawn LW_fnc_borderGuard;
 *
 *   - Trigger representing outer border:
 *     - Name: LW_OuterBorder
 *     - Shape: Rectangle
 *     - Type: None
 *     - Activation: Anybody - Repeatedly - Present
 *     - Condition: alive player && !(player in thisList)
 *     - On Activation: _null = [] spawn LW_fnc_borderGuardExecution;
 *
 *   = = = = = = = = = = = = = = = = = = = = = =
 *
 *   Multiple Borders:
 *   - Create two triggers
 *
 *   - Trigger representing inner border:
 *     - Name: LW_InnerBorder
 *     - Shape: Rectangle
 *     - Type: None
 *     - Activation: Anybody - Repeatedly - Present
 *     - Condition: alive player && player in thisList
 *     - On Activation: LW_scr_borderGuard = [] spawn LW_fnc_borderGuard;
 *     - On Deactivation: call LW_fnc_borderGuardTerminator;
 *
 *   - Trigger representing outer border:
 *     - Name: LW_OuterBorder
 *     - Shape: Rectangle
 *     - Type: None
 *     - Activation: Anybody - Repeatedly - Present
 *     - Condition: alive player && player in thisList
 *     - On Activation: _null = [] spawn LW_fnc_borderGuardExecution;
 *
 *   - Copy thous around your combat zone
 *
 *   - Create BorderGuardMarker:
 *     - Based on shape of your combat zone You have to create at least one
 *       BorderGuardMarker with name 'LW_BorderGuardMarker' if You need more
 *       markers use 'LW_BorderGuardMarker_#'
 *     - Place them in the middle of the combat zone (if player cross the borders
 *       he is tasked to move in direction of nearest BorderGuardMarker)
 *
 *   = = = = = = = = = = = = = = = = = = = = = =
 *
 *   On Mission End:
 *   BorderGuard is ready to complete the task created on mission initialization
 *   all what You have to do is '_null = [] spawn LW_fnc_borderGuardMissionTaskComplete;'
 *
 *
 *   *** *** *** *** *** *** *** *** *** *** ***
 *
 *   All globals defined here and in all my other scripts begins with 'LW_'.
 *
 *   Variables with prefix 'LW_par_' represents values that can be assigned from 'paramsArray'.
 *
 *   Variables with prefix 'LW_var_' should be set once You create the mission.
 *
 *   Note: Do not change values of variables under the 'private line'.
 *
 */

// BorderGuard
// Protect borders with Border Guard [ DoNothing: -1 | Warn: 0 | Warn&Task: 1 | Task: 2 ]
LW_par_borderGuard = 1;

// BorderGuard Action Delay
// This delay take place after task assignment and can be delayed by 'LW_par_borderGuardTaskDelay'
// If player stay in/out of the trigger for this duration it's considered as border violation
// Value in seconds [ Turn Off later actions: 0 ]
LW_par_borderGuardActionDelay = 5;

// BorderGuard May Kill
// If You're using Border Guard with task this should be always on [ there is no other punishment for failed task ]
// When turned off Border Gurad flash the warning message as the action against violent player 'LW_par_borderGuardActionDelay'
// Allow execution [ DoNothing: 0 | Kill: 1 ]
LW_par_borderGuardMayKill = 1;

// BorderGuard Task Delay
// This option is valid only if You're using Border Guard with task
// Delay between Border Guard activation and task assignment
// This will also delay the 'LW_par_borderGuardActionDelay'
// Value in seconds
LW_par_borderGuardTaskDelay = 5;

// BorderGuard Task Distance
// This option is valid only if You're using Border Guard with task
// Distance over which player have to move back in direction of 'LW_var_borderGuardSimpleBorders'
// Value in meters
LW_par_borderGuardTaskDistance = 50;

// BorderGuard Task Duration
// This option is valid only if You're using Border Guard with task
// Time in which player have to move in desired direction of 'LW_var_borderGuardSimpleBorders'
// for given distance 'LW_par_borderGuardTaskDistance' to successfully complete the task
// Value in seconds
LW_par_borderGuardTaskDuration = 15;

// BorderGuard Warning
// This aplies only for initial warning when player activates the Border Guard
// Text will be displayed as 'titleText'
// Border warning style [ Text: 0 | Hint: 1 ]
LW_par_borderGuardWarning = 0;

/*
 *   Additional Options
 *
 *   Keep this variables here !
 *
 */

// BorderGuard Simple Borders
// This option is valid only if You're using Border Guard with task
// True: Two triggers named 'LW_InnerBorder' and 'LW_OuterBorder', no markers
// False: Multiple triggers, marker(s) in center of map named 'LW_BorderGuardMarker'
LW_var_borderGuardSimpleBorders = true;

// BorderGuard Create Borders
// Use this option if You wish to create visible borders around your combat zone.
// If You use Simple Borders, markers will be created around your combat zone.
// If You use Multiple Borders, markers will be created over the triggers listed
// in array 'LW_var_borderGuardOuterBorders'.
LW_var_borderGuardCreateBorders = true;

// BorderGuard Outer Borders
// This option is valid only if You're using Multiple Borders
// List of triggers representing outer borders
LW_var_borderGuardOuterBorders = [];

// BorderGuard Border Color
// List of available colors at: https://community.bistudio.com/wiki/setMarkerColor
LW_var_borderGuardBorderColor = "ColorRed";

// BorderGuard Border Brush
// List of available brushes at: https://community.bistudio.com/wiki/setMarkerBrush
LW_var_borderGuardBorderBrush = "Solid";

// BorderGuard Border Alpha
// Transparency [ Min: 1 | Max: 0 ]
LW_var_borderGuardBorderAlpha = 0.7;

// BorderGuard Border Width
// This option is valid only if You're using Simple Borders
// Width of borders created around your combat zone
LW_var_borderGuardBorderWidth = 5;

// BorderGuard System Messages
// Send messages to system chat [ locally ]
LW_var_borderGuardSystemMessages = false;

/*
 *   Private Line
 *
 *   Do not change anything below ! There is nothing what You can tune up
 *
 */

LW_fnc_borderGuard = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuard.sqf");
LW_fnc_borderGuardExecution = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardExecution.sqf");
LW_fnc_borderGuardMissionTaskComplete = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardMissionTaskComplete.sqf");
LW_fnc_borderGuardTerminator = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardTerminator.sqf");

if (!isDedicated) then
{
  if (LW_par_borderGuard > -1) then
  {
    LW_fnc_borderGuardMissionTask = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardMissionTask.sqf");
    LW_fnc_borderGuardWarning = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardWarning.sqf");

    if (LW_par_borderGuard > 0) then
    {
      LW_fnc_borderGuardCheckDistance = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardCheckDistance.sqf");
      LW_fnc_borderGuardMarkers = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardMarkers.sqf");
      LW_fnc_borderGuardTask = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardTask.sqf");
      LW_fnc_borderGuardTaskComplete = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardTaskComplete.sqf");
      LW_fnc_borderGuardTaskRemove = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardTaskRemove.sqf");
    };

    [] spawn
    {
      private "_null";

      waitUntil { !isNull player && isPlayer player }; // ASAP

      _null = [] spawn LW_fnc_borderGuardMissionTask;

      if !LW_var_borderGuardSimpleBorders then
      {
        LW_var_borderGuardMarkers = call LW_fnc_borderGuardMarkers;
      };
    };
  };

  if LW_var_borderGuardCreateBorders then
  {
    LW_fnc_borderGuardBorders = compileFinal preprocessFileLineNumbers (_LW_path_functionsBorderGuard + "LW_fnc_borderGuardBorders.sqf");

    [] spawn
    {
      private "_null";

      waitUntil { !isNull player && isPlayer player }; // ASAP

      _null = [] spawn LW_fnc_borderGuardBorders;
    };
  };
};

LW_var_borderGuardCountActivation = 0;
LW_var_borderGuardCountExecution = 0;
LW_var_borderGuardCountTask = 0;
LW_var_borderGuardCountViolation = 0;

LW_var_borderGuardTaskActive = false;

LW_var_borderGuardVersion = 1.3;

_LW_path_functionsBorderGuard = nil;
