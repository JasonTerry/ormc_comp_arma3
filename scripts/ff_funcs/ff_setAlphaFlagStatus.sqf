// =============================================================================
//  This setter takes in the side given, and handles the increase or decres in control of
//  it's governing point.
// =============================================================================
_unit = _this select 0;
systemChat "Checking at ALPHA, inside setStatus";

systemChat format["Found at alpha: %1", side _unit];
// BLUE LOGIC
if (side _unit isEqualTo west) then {

  // if EAST is greater than 0, decrease EAST
  if (APLHA_OWNERSHIP_EAST > 0) then {

      APLHA_OWNERSHIP_EAST = APLHA_OWNERSHIP_EAST - 1; // dec by 1

  };

  // if EAST is at 0, and WEST is less than 10, increase WEST
  if ((APLHA_OWNERSHIP_EAST == 0) && (APLHA_OWNERSHIP_WEST < 10)) then {

      APLHA_OWNERSHIP_WEST = APLHA_OWNERSHIP_WEST + 1; // inc by 1

  };

};

// RED LOGIC
if (side _unit isEqualTo east) then {

  // if EAST is greater than 0, decrease EAST
  if (APLHA_OWNERSHIP_WEST > 0) then {

      APLHA_OWNERSHIP_WEST = APLHA_OWNERSHIP_WEST - 1; // dec by 1

  };

  // if WEST is at 0, and EAST is less than 10, increase EAST
  if ((APLHA_OWNERSHIP_WEST == 0) && (APLHA_OWNERSHIP_EAST < 10)) then {

      APLHA_OWNERSHIP_EAST = APLHA_OWNERSHIP_EAST + 1; // inc by 1

  };

};

systemChat format["Alpha Ownership East = %1", APLHA_OWNERSHIP_EAST];
systemChat format["Alpha Ownership West = %1", APLHA_OWNERSHIP_WEST];
