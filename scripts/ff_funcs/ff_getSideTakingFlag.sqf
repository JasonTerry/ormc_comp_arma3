private ["_flag", "_westcnt", "_eastcnt"];

_flag = _this select 0;
_westcnt = _this select 1;
_eastcnt = _this select 2;
_side = "";
systemChat format ["_flag = %1, wstcnt = %2, estcnt = %3", _flag, _westcnt, _eastcnt];

if (_westcnt > _eastcnt) then {
  _side = "Blue Team"; // West is capturing
  systemChat format ["The %1 is taking %2!", _side, _flag];
};

if (_westcnt < _eastcnt) then {
  _side = "Red Team"; // East is capturing
  systemChat format ["The %1 is taking %2!", _side, _flag];
};

if (_westcnt == _eastcnt) then {
  _side = "contested";
  systemChat format ["%2 is %1!", _side, _flag];
};

_side
// return side capturing the point or return point contested.
