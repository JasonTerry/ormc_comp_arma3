// =============================================================================
//  [ff_actionPhaseEndCheck.sqf]
//  Takes no arguments, returns a boolean.
//  This script checks for all possible end conditions for the action phase.
//  If the ActionPhase should end, it should return TRUE, otherwise it should
// These checks are NOT related to the timer. Which is handled in the loop.
//  Return false
// =============================================================================

if (CLOCK_ELAPSED > ROUND_TIME) then {
    systemChat format["TIME UP! ENDING ACTION PHASE!"];
    ACTION_PHASE = false;
}


// Check if everyone is dead on one side
// Check if all points are under control of one side
