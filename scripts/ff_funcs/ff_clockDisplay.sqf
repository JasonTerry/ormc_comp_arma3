// =============================================================================
//  [ff_clockDisplay.sqf]
//  Takes no arguments, is used to calculate time remaining in any of the possible states of the game.
//  This will assume the only one of the below if statements will EVER be true.
// =============================================================================

if (ACTION_PHASE) then {
  // calc action phase time
  CLOCK_DISPLAY = ROUND_TIME - CLOCK_ELAPSED;
};

if (PREP_PHASE) then {
  // calc prep phase time
  CLOCK_DISPLAY = PREP_TIME - CLOCK_ELAPSED;
};

if (floor CLOCK_DISPLAY != floor CLOCK_DISPLAY_LAST) then {
  _timeLeft = [CLOCK_DISPLAY, "MM:SS"] call BIS_fnc_secondsToString;
  systemChat format ["Time Elapsed | %1 " , _timeLeft];
};

CLOCK_DISPLAY_LAST = CLOCK_DISPLAY;
