// =============================================================================  //
//  [ff_enableSimulation.sqf]                                                     //
//  MP command. Enable or disable simulation for given entity, globally.          //
//  Call this only from the server.                                               //
//  Has the same effect as enableSimulation when used in SP.                      //
// =============================================================================  //

{ // forEach _players
  _x enableSimulationGlobal true;
  player reveal _x;
  systemChat format["enableSimulation"];
} forEach TOTAL_CURRENT_PLAYERS;




//Units that have been previously subjected to enableSimulation false;
//or enableSimulationGlobal false;
//may stay unrecognised for a long time even after simulation was re-enabled,
//returning objNull as cursorTarget.
//Force revealing units with reveal command usually solves the problem.
//For example:
// {player reveal _x} forEach allUnits;
