// =============================================================================
//  [ff_clockTimer.sqf]
//  Takes no arguments, called during the loop in which it manages. It updates the TIME_COUNTDOWN
//  to be the elpased time since CLOCK_START was last set.
// =============================================================================
CLOCK_ELAPSED = serverTime - CLOCK_START;
