// =============================================================================
//  [ff_clockSet.sqf]
//  Takes no arguments, can be called to set the CLOCK_START in order to calculate
//  time left in the round, or other time managed events.
// =============================================================================
CLOCK_START = serverTime;
CLOCK_ELAPSED = 0;
CLOCK_DISPLAY = 0;
_clockStart = [CLOCK_START, "MM:SS"] call BIS_fnc_secondsToString;
systemChat format ["Last Time State | %1 | %2", _clockStart, CLOCK_START];
