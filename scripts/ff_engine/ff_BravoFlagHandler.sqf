// control flow for capture points. This is called at the start of a round and runs throughout
// this is the ALPHA flag control flow.

// init bravo
_debug1 = false;
_debug2 = false;

while {ACTION_PHASE} do {
  { // start of forEach _players
    if (_debug1 isEqualTo false) then {
      systemChat "CHECKING FOR PLAYERS AT BRAVO";
      sleep 0.5;
      _debug1 = true;
    };
    if (!isNull _x && {isPlayer _x}) then {
      // check distance of all players to that of the point.
      if (_debug2 isEqualTo false) then {
        systemChat format ["%1 is of side %2", _x, (side _x)];
        sleep 0.5;
        _debug2 = true;
      };
      if (_x distance (getPos BRAVO_POLE) < CAP_RADIUS) then {
        // This bit should be replaced with a function that returns the dominate side on the flag.
        if (side _x isEqualTo west) then {
          // assign side of current obj to _side
          _side = side _x;
          // pass side of current object to setter func for bravo status
          [_side] call ff_setBravoFlagStatus;
        };
        if (side _x isEqualTo east) then {
          // assign side of current obj to _side
          _side = side _x;
          // pass side of current object to setter func for bravo status
          [_side] call ff_setBravoFlagStatus;
        };
      };
    };
  } forEach TOTAL_CURRENT_PLAYERS;
  sleep 2;
};
true
