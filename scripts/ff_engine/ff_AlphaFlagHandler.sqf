// control flow for capture points. This is called at the start of a round and runs throughout
// this is the ALPHA flag control flow.

// init alpha
_debug1 = false;

while {ACTION_PHASE} do {

  {  // start of forEach _players

    if (_debug1 isEqualTo false) then {
      systemChat "CHECKING FOR PLAYERS AT ALPHA";
      systemChat format ["PLAYERS = %1", TOTAL_CURRENT_PLAYERS];
      sleep 0.5;
      _debug1 = true;
    };

    systemChat format ["PLAYERS = %1", TOTAL_CURRENT_PLAYERS];

    if (!(isNull _x) && (isPlayer _x) && (alive _x)) then {
      systemChat format["Checking for %1", _x];
      // check distance of all players to that of the point.
      if (_x distance (getPos ALPHA_POLE) < CAP_RADIUS) then {

          systemChat "SOMEONE IS ON ALPHA!";
          [_x] call ff_setAlphaFlagStatus;
      }; // if in range of point
    }; // if not null

  } forEach TOTAL_CURRENT_PLAYERS;

  systemChat format["HANDLER LOOP: Alpha Ownership East = %1", APLHA_OWNERSHIP_EAST];
  systemChat format["HANDLER LOOP: Alpha Ownership West = %1", APLHA_OWNERSHIP_WEST];

  sleep CAPTURE_INCR;

};
