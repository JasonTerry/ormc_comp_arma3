// THIS SCRIPT SHOULD STEP THROUGH THE ENTIRE MATCH TO INCLUDE RESTARTING THE SERVER!

systemChat "MAIN LOOP START";

while {!WARMUP_OVER} do {
  call ff_unitCheck;
  _pcnt = count TOTAL_CURRENT_PLAYERS;
  systemChat format ["COUNT OF PLAYERS = %1", _pcnt];
  sleep LOOP_SLP;
  if (_pcnt >= 1) then {
    WARMUP_OVER = true;
    systemChat "WARMUP IS OVER!";
    sleep LOOP_SLP;
    SETUP_PHASE = true;
    systemChat "SETUP IS NOW TRUE!";
    sleep LOOP_SLP;
  };
};

// SETUP OF MATCH HAPPENS WITHIN WAIT UNTIL
// call ff_clock;
[] call ff_setup;
waitUntil {MATCH_READY};


// THE ACTUAL GAME LOOP | CONSIST OF TWO INTERNAL LOOPS
if (!isServer) exitWith {systemChat "EXITING SERVER SCRIPT"};

while {CURRENT_ROUND < MATCH_LENGTH} do {

  // countdown to prep phase. Possible error catching here?
  systemChat format ["ROUND %1 PREP_PHASE!", CURRENT_ROUND];
  PREP_PHASE = true;
  ACTION_PHASE = false;

  sleep READY_CHECK;

  // TODO:: THIS LOOP WILL RELATE TO A COUNT DOWN TIMER
  call ff_clockSet;
  call ff_clockTimer;
  //respawn function goes here
  call ff_disableSimulation;
  while {(PREP_PHASE) && (CLOCK_ELAPSED < PREP_TIME)} do {

    // SET PLAYER CUR BASED ON TEAM CUR

    call ff_clockTimer; // updates elapsed time
    call ff_clockDisplay; // displays phase count down

    sleep LOOP_SLP;
  };
  // TODO:: THIS LOOP WILL RELATE TO A COUNT DOWN TIMER

  // setup action phase
  systemChat format ["ROUND %1 ACTION_PHASE!", CURRENT_ROUND];
  PREP_PHASE = false;
  ACTION_PHASE = true;
  sleep READY_CHECK;

  // Need function ff_setupActionPhase.sqf
  // START FLAG LOOPS
  // TODO:: SHOULD THESE BE LOOPS ARE SINGLE FIRED SCRIPTS?
  [] spawn ff_AlphaFlagHandler; // START ALPHA FLAG LOOP, ends on ACTION_PHASE == false
  // [] spawn ff_BravoFlagHandler; // START BRAVO FLAG LOOP
  // [] spawn ff_CharlieFlagHandler; // START CHARLIE FLAG LOOP

  // TODO:: THIS LOOP WILL RELATE TO A COUNT DOWN TIMER
  call ff_clockSet;
  call ff_clockTimer;
  call ff_enableSimulation;
  while {ACTION_PHASE} do {
    // INVOKE ACTION PHASE + START TIMER
    // Update the elapsed time
    // check if the actionPhase should end.
    // if true, skip all action phase logic, end the phase.
    // Update The HUD
    // Check The Flags

    sleep LOOP_SLP;
    // Update the elapsed time
    call ff_clockTimer; // updates elapsed time
    call ff_clockDisplay; // displays phase count down

    // func to check if action phase should end
    call ff_actionPhaseEndCheck;
    // If all players on a side are dead, or time is up.
    // return true or false
  };

  // clean up last round/phase HandleScore

  // need a func to delete all corpses on the map
  // need a func to delete all wrecks on map
  CURRENT_ROUND = CURRENT_ROUND + 1;


};

systemChat format ["Match is now over!"];
MATCH_OVER = true;
MATCH_OVER
// record all stats to DB, clean up, restart server.
