// ===========================================================================
// COMPETITIVE ARMA 3 FIRE FIGHT v 0.1a  | By: Jason Terry
// [ff_core.sqf] is the data-bank for the entire FF engine. Everything
// pulls from ff_core and feeds back to it in some way.
// ============================================================================


// Team Points, stored in a array, each index is a ROUND
BLUE_POINTS = [0,0,0,0,0];
RED_POINTS = [0,0,0,0,0];

// List of the players on each team.
BLUE_TEAM = 0;
RED_TEAM = 0;

BASE_CUR = 50; // BASE APPLIED AT THE START OF EVERY ROUND
BLUE_CUR = [50,0,0,0,0]; // ARRAY OF CUR AVAILABLE FOR THE ROUND
RED_CUR = [50,0,0,0,0]; // ARRAY OF CUR AVAILABLE FOR THE ROUND

// FLAG OWNERSHIP GLOBALS
// Scales from 0 - 10 for each side
// For each player on the point, control for that side increments by 1.
// Control my not increase if the opposing team is not at zero.
CAPTURE_INCR = 1;
APLHA_OWNERSHIP_WEST = 0;
APLHA_OWNERSHIP_EAST = 0;
BRAVO_OWNERSHIP_WEST = 0;
BRAVO_OWNERSHIP_EAST = 0;
CHARLIE_OWNERSHIP_WEST = 0;
CHARLIE_OWNERSHIP_EAST = 0;

// FLAG STATE GLOBALS ---
// 1. west, 2. east, 3. neut.
ALPHA_STATUS = "neut";
BRAVO_STATUS = "neut";
CHARLIE_STATUS = "neut";

// For setting the HUD point displays according to the above.
ALPHA_DISPLAY_STATUS = "data\images\rsc\sectorA_neut.paa";
BRAVO_DISPLAY_STATUS = "data\images\rsc\sectorB_neut.paa";
CHARLIE_DISPLAY_STATUS = "data\images\rsc\sectorC_neut.paa";

// CAP RADIUS, centered on flag pole.
CAP_RADIUS = 15; // 15 meters

// ROUNDS PER MATCH
MATCH_LENGTH = 5; // 5 Is the defualt value, but 3 is recomended for public servers.

// PRESET TIMER LENGTHS
ROUND_TIME = 75; // 7 minutes should be the default
PREP_TIME = 15; // 15 seconds should be the default
READY_CHECK = 5; // 5 second wait should be commonly used. May need to be increased?

// LOGIC CONTROL GLOBALS
TOTAL_CURRENT_PLAYERS = 0;

CURRENT_ROUND = 0;
WARMUP_OVER = false;
SETUP_PHASE = false;
MATCH_READY = false;
PREP_PHASE = false;
ACTION_PHASE = false;
MATCH_OVER = false;
// TIME CONTROL GLOBALS
CLOCK_RUNNING = false;
CLOCK_DISPLAY = 0;
CLOCK_DISPLAY_LAST = 0;
CLOCK_START = 0;
CLOCK_ELAPSED = 0;

// HELPER GLOBALS
LOOP_SLP = 0.125;


////////////////////////////////////////////////////////////////////////////////
// DO NOT EDIT BELOW THIS LINE -------------------------------------------------
////////////////////////////////////////////////////////////////////////////////

// Returns the total players connected.
ff_unitCheck = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_unitCheck.sqf";

// Takes two ints and returns a side.
ff_getSideTakingFlag = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_getSideTakingFlag.sqf";

// Clock Logic
ff_clockSet = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_clockSet.sqf";
ff_clockTimer = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_clockTimer.sqf";
ff_clockDisplay = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_clockDisplay.sqf";

// FLAG HANDLERS
ff_AlphaFlagHandler = compileFinal preprocessFileLineNumbers "scripts\ff_engine\ff_AlphaFlagHandler.sqf";
ff_BravoFlagHandler = compileFinal preprocessFileLineNumbers "scripts\ff_engine\ff_BravoFlagHandler.sqf";
ff_CharlieFlagHandler = compileFinal preprocessFileLineNumbers "scripts\ff_engine\ff_CharlieFlagHandler.sqf";
ff_setAlphaFlagStatus = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_setAlphaFlagStatus.sqf";

// The MAIN Loop
ff_main = compileFinal preprocessFileLineNumbers "scripts\ff_engine\ff_main.sqf";

// Phase Control Functions
ff_actionPhaseEndCheck = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_actionPhaseEndCheck.sqf";
ff_enableSimulation = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_enableSimulation.sqf";
ff_disableSimulation = compileFinal preprocessFileLineNumbers "scripts\ff_funcs\ff_disableSimulation.sqf";
// Sets up the match once all conditions are met.
ff_setup = compileFinal  preprocessFile "scripts\ff_engine\ff_setup.sqf";
