// ff_setup
// Handles all prep work for match.
// INIT THE MATCH! | KILL AND RESPAWN ALL PLAYERS AT START POINTS | INVOKE TASK AND INTRO CUTSCENE
// -- DURING THE ABOVE SET TEAM_CUR, INIT ALL STATS TO 0, ETC



systemChat "Now running SETUP";
CURRENT_ROUND = 1;

systemChat "ROUND 1 READY!";
sleep 1;

SETUP_PHASE = false;
MATCH_READY = true;

systemChat "SETUP COMPLETE!";
sleep 1;

systemChat "MATCH IS NOW READY!";
sleep 1;
