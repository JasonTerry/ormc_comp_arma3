// control flow for capture points. This is called at the start of a round and runs throughout
// this is the ALPHA flag control flow.

// init charile
_debug1 = false;

while {ACTION_PHASE} do {
  { // start of forEach _players
    if (_debug1 isEqualTo false) then {
      systemChat "CHECKING FOR PLAYERS AT CHARLIE";
      sleep 0.5;
      _debug1 = true;
    };
    if (!isNull _x && {isPlayer _x}) then {
      // check distance of all players to that of the point.
      if (_debug1 isEqualTo false) then {
        systemChat format ["%1 is of side %2", _x, (side _x)];
        sleep 0.5;
        _debug2 = true;
      };
      if (_x distance (getPos CHARLIE_POLE) < CAP_RADIUS) then {
        // This bit should be replaced with a function that returns the dominate side on the flag.
        if (side _x isEqualTo west) then {
          // assign side of current obj to _side
          _side = side _x;
          // pass side of current object to setter func for charlie status
          [_side] call ff_setCharlieFlagStatus;
        };
        if (side _x isEqualTo east) then {
          // assign side of current obj to _side
          _side = side _x;
          // pass side of current object to setter func for charlie status
          [_side] call ff_setCharlieFlagStatus;
        };
      };
    }; // End
  } forEach TOTAL_CURRENT_PLAYERS;
  sleep 2;
}; // while loop
true
