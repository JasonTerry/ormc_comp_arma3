// First set the respawn timer to the desired amount.
// Currently we are simply setting it to the time remaining for the round
if (ACTION_PHASE) then {
  hint format ["setting repspawn timer | %1", (ROUND_TIME - CLOCK_ELAPSED) ];
  systemchat format ["setting repspawn timer | %1", (ROUND_TIME - CLOCK_ELAPSED) ];
  setPlayerRespawnTime (ROUND_TIME - CLOCK_ELAPSED);
}

// Next we call the BIS_fnc_EGSpectator, this puts them into spectator while they wait.
["Initialize",
[player, //unit being initialized
[side player], //sides that can be spectated
false, //can AI be viewed
false, //can free camera be used
false, //can 3rd person be used
false, //show focus info widgets
false, //show camera button widgets or not
true, //Show controls helper widget
true, //whether to show header widget
true //Whether to show entities/locations lists
]] call BIS_fnc_EGSpectator;
hint "spectator mode has started";
systemchat "spectator mode has started";

sleep 3; // ???
// What does this do?
BIS_DeathBlur ppEffectAdjust [0.0];
BIS_DeathBlur ppEffectCommit 0.0;
