class RscPicture
{
 access = 0;
 type = CT_STATIC;
 idc = -1;
 style = ST_PICTURE; //ST_PICTURE
 colorBackground[] = {0,0,0,0};
 colorText[] = {1,1,1,1};
 font = "TahomaB";
 sizeEx = 0;
 lineSpacing = 0;
 text = "";
 fixedWidth = 0;
 shadow = 0;
};

class RscStructuredText
{
 access = 0;
 type = CT_STATIC;
 idc = -1;
 style = ST_CENTER;
 colorBackground[] = {0,0,0,0};
 colorText[] = {1,1,1,1};
 w = 0.1; h = 0.05;
 //x and y are not part of a global class since each rsctext will be positioned 'somewhere'
 font = "TahomaB";
 sizeEx = 0.05;
 lineSpacing = 0;
 text = "";
 fixedWidth = 0;
 shadow = 0;
};
// Everything above this line is base classes.

class RscTitles {

  /*class FFFeed
  {
    idd = 331;
    duration = 1e+1000;
    fadeIn = 0;
    fadeOut = 0;
    name = "";
    onLoad = "uiNamespace setVariable ['FFFeed_Label', _this select 0];";
    class controls {
      class ff_Feed: RscPicture
      {
        idc = 1300;
        text = "#(argb,8,8,3)color(1,1,1,1)";
        x = 0.0103247 * safezoneW + safezoneX;
        y = 0.0161914 * safezoneH + safezoneY;
        w = 0.159789 * safezoneW;
        h = 0.0769695 * safezoneH;
      };
    }; // end ffeed controls
  }; // end ffeed

  class FFClock
  {
    idd = 332;
    duration = 1e+1000;
    fadeIn = 0;
    fadeOut = 0;
    name = "";
    onLoad = "uiNamespace setVariable ['FFFeed_Label', _this select 0];";
    class controls {
      class ff_GameClock: RscPicture
      {
      	idc = 1304;
      	text = "data\images\rsc\timer_frame.paa";
      	x = 0.876277 * safezoneW + safezoneX;
      	y = 0.92883 * safezoneH + safezoneY;
      	w = 0.113398 * safezoneW;
      	h = 0.0549782 * safezoneH;
      };
      class ff_GameClockTime: RscStructuredText
      {
        idc = 1305;
        text = "Test";
        x = 0.899277 * safezoneW + safezoneX;
        y = 0.93883 * safezoneH + safezoneY;
      };
    }; // end ffclock controls
  }; // end ffclock*/

  class FFDisplay //Dialog class name, gets called by: handle = CreateDialog "name"
  {
    idd = 333;
    duration = 1e+1000;
    fadeIn = 0;
    fadeOut = 0;
    name = "";
    onLoad = "uiNamespace setVariable ['FFDisplay_Label', _this select 0];";
    class controls {
      class ff_AlphaStatus: RscPicture
      {
      	idc = 1301;
      	text = "data\images\rsc\sectorA_neut.paa";
      	x = 0.386602 * safezoneW + safezoneX;
      	y = 0.00519575 * safezoneH + safezoneY;
      	w = 0.0618537 * safezoneW;
      	h = 0.0879652 * safezoneH;
      };
      class ff_BravoStatus: RscPicture
      {
        idc = 1302;
        text = "data\images\rsc\sectorB_neut.paa";
        x = 0.469073 * safezoneW + safezoneX;
        y = 0.00519575 * safezoneH + safezoneY;
        w = 0.0618537 * safezoneW;
        h = 0.0879652 * safezoneH;
      };
      class ff_CharlieStatus: RscPicture
      {
        idc = 1303;
        text = "data\images\rsc\sectorC_neut.paa";
        x = 0.551545 * safezoneW + safezoneX;
        y = 0.00519575 * safezoneH + safezoneY;
        w = 0.0618537 * safezoneW;
        h = 0.0879652 * safezoneH;
      };
    };//end: controls class
  };//end: FFDisplay class

};//en: rscTitles class
