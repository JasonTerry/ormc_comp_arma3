>>>>--[RED DEFUALT LOADOUT]--
RED_SECD = hgun_Rook40_F [DEFUALT] // Rook 9mm, always spawn with this.
RED_SECD_MAG = 16Rnd_9x21_Mag // 16 rnd 9mm, spawn with 2 mags always

>>>>--[RED EQUIPMENT]--
[RED ALL ROLES]
RED_T1_VEST = V_Chestrig_blk [DEFAULT] // No protection, just carries gear
RED_T2_VEST = V_TacVest_blk [20] // Minimal Ballistic Protection.
RED_T3_VEST = V_PlateCarrier1_blk [40] // Moderate Ballistic Protection
RED_T4_VEST = V_PlateCarrier2_blk [80] // Maximum Ballistic Protection

[RED TL ROLE] --
RED_T1_VEH = O_Quadbike_01_F [25] // "Opfor" paint
RED_T2_VEH = O_G_Offroad_01_repair_F [50] // "Guerilla_12" paint
RED_T3_VEH = I_G_Offroad_01_armed_F [100] // "Guerilla_12" paint

[RED MARKSMEN ROLE] --
RED_GSUIT = U_O_FullGhillie_lsh [80]

[RED SUPPORT ROLE] --
RED_PACK_SMALL = B_AssaultPack_ocamo [20] // can hold up to 3 extra boxes
RED_PACK_LARGE = B_Carryall_ocamo [40] // can hold up to 4 extra boxes

>>>>--[BOTH TEAMS ALL ROLES]--

FRAG GRENADES [10] // Max of 2
WHITE SMOKE [5] // Max of 4
EXTRA MAGS [5] // No Max always pulls from current weapons listed mag type
EXTRA MAGS SECD [5] // No Max allways pull from current secondary listed mag type
MEDKIT [10] // No Max
AGL_HE [5] = 1Rnd_HE_Grenade_shell // Max of 4 HE 40mm
AGL_WSM [5] = 1Rnd_Smoke_Grenade_shell // Max of 4 White Smoke 40mm

>>>>--[WEAPONS AND OPTICS]--

[RED ALL ROLES]
RED_SECD = hgun_Rook40_F [DEFUALT] // Rook 9mm, always spawn with this.
RED_SECD_MAG = 16Rnd_9x21_Mag // 16 rnd 9mm, spawn with 2 mags always

[RED TL ROLE] --
RED_T1_SMG = SMG_02_F [20] // STING 9mm
RED_T1_SMG_MAG = 30Rnd_9x21_Mag // STING 9mm

RED_T1_ARGL = arifle_TRG21_GL_F [30] // TRG20 5.56 GL
RED_T1_ARGL_MAG = 30Rnd_556x45_Stanag // TRG20

RED_T2_ARGL = arifle_Katiba_GL_F [40] // KABITA KARBINE 6.65 GL
RED_T2_ARGL_MAG = 30Rnd_65x39_caseless_green // KABITA KARBINE 6.65

RED_T3_AR = srifle_DMR_03_F [60] // MK-1 EMR Black 7.62
RED_T3_AR_MAG = 20Rnd_762x51_Mag // MK-1 EMR

RED_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
RED_T2_OPTIC = optic_Hamr [40] // 4x Optic
RED_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic
RED_T4_OPTIC = optic_KHS_old [60] // Best optic available but costly

[RED RIFLEMEN ROLE] --
RED_T1_SMG = SMG_02_F [20] // STING 9mm
RED_T1_SMG_MAG = 30Rnd_9x21_Mag // STING 9mm

RED_T1_AR = arifle_TRG20_F [30] // TRG20 5.56
RED_T1_AR_MAG = 30Rnd_556x45_Stanag // TRG20 5.56

RED_T2_AR = arifle_Katiba_C_F [40] // KABITA KARBINE 6.65
RED_T2_AR_MAG = 30Rnd_65x39_caseless_green // KABITA KARBINE 6.65

RED_T3_AR = srifle_DMR_03_F [60] // MK-1 EMR Black 7.62
RED_T3_AR_MAG = 20Rnd_762x51_Mag // MK-1 EMR

RED_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
RED_T2_OPTIC = optic_Hamr [40] // 4x Optic
RED_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic

[RED MARKSMEN ROLE] --
RED_T1_SMG = SMG_02_F [30] // STING 9mm
RED_T1_SMG_MAG = 30Rnd_9x21_Mag // STING 9mm

RED_T1_DMR = srifle_DMR_05_hex [40] // Cyrus HEX 9.3mm
RED_T1_DMR_MAG = 10Rnd_93x64_DMR_05_Mag // Cyrus HEX 9.3mm

RED_T2_DMR = srifle_DMR_02_F [50] // MAR 10 BLACK .338
RED_T2_DMR_MAG = 10Rnd_338_Mag // MAR 10 .338

RED_T3_DMR = srifle_LRR_F [60] // m320 LRR Black .408
RED_T3_DMR_MAG = 7Rnd_408_Mag // M320 .408

RED_T4_OPTIC = optic_KHS_old [30] // Only Optic Available for Marksmen

[RED SUPPORT ROLE] --
RED_T1_SMG = SMG_02_F [30] // STING 9mm
RED_T1_SMG_MAG = 30Rnd_9x21_Mag // STING 9mm

RED_T1_LMG = LMG_Zafir_F [40] // Zafir 7.62
RED_T1_LMG_MAG = 150Rnd_762x54_Box // Zafir

RED_T2_LMG = MMG_01_hex_F [50] // Navid HEX
RED_T2_LMG_MAG = 150Rnd_93x64_Mag // Navid

RED_T3_LMG = MMG_02_black_F [60] // MMG
RED_T3_LMG_MAG = 130Rnd_338_Mag // MMG

RED_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
RED_T2_OPTIC = optic_Hamr [40] // 4x Optic
RED_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic
