/* --[BLU DEFUALT LOADOUT]--
BLUE_SECD = hgun_P07_F [DEFUALT] // P07 9mm
2x BLUE_SECD_MAG = 16Rnd_9x21_Mag // 16 rnd 9mm, spawn with 2 mags always
*/

// --[BLU EQUIPMENT]--
// [BLU ALL ROLES]
BLU_T1_VEST = "V_Chestrig_oli" // [10] // No protection, just carries gear
BLU_T2_VEST = "V_TacVest_oli" // [20] // Minimal Ballistic Protection.
BLU_T3_VEST = "V_PlateCarrier1_rgr" // [40] // Moderate Ballistic Protection
BLU_T4_VEST = "V_PlateCarrier2_rgr" // [80] // Maximum Ballistic Protection

// [BLU TL ROLE] --
BLU_T1_VEH = "O_Quadbike_01_F" // [25] // "Blufor" paint
BLU_T2_VEH = "O_G_Offroad_01_repair_F" // [50] // "Guerilla_05" paint
BLU_T3_VEH = "I_G_Offroad_01_armed_F" // [100] // "Guerilla_05" paint

// [BLU MARKSMEN ROLE] --
BLU_GSUIT = "U_B_FullGhillie_lsh" // [80]

// [BLU SUPPORT ROLE] --
BLU_PACK_SMALL = "B_AssaultPack_oli" // [20] // can hold up to 3 extra boxes
BLU_PACK_LARGE = "B_Carryall_oli" // [40] // can hold up to 4 extra boxes

>>>>--[BOTH TEAMS ALL ROLES]--

FRAG GRENADES [10] // Max of 2
WHITE SMOKE [5] // Max of 4
EXTRA MAGS PRIM [5] // No Max always pulls from current weapons listed mag type
EXTRA MAGS SECD [5] // No Max allways pull from current secondary listed mag type
MEDKIT [10] // No Max
AGL_HE [15] = "1Rnd_HE_Grenade_shell" // Max of 4 HE 40mm
AGL_WSM [15] = "1Rnd_Smoke_Grenade_shell" // Max of 4 White Smoke 40mm

>>>>--[WEAPONS AND OPTICS]--

[BLU ALL ROLES]
BLUE_SECD = hgun_P07_F [DEFUALT] // P07 9mm, always spawn with this.
BLUE_SECD_MAG = 16Rnd_9x21_Mag // 16 rnd 9mm, spawn with 2 mags always

[BLU TL ROLE] --
BLU_T1_SMG = SMG_01_F [20] // Vermin 45acp
BLU_T1_SMG_MAG = 30Rnd_45ACP_Mag_SMG_01 // Vermin 45acp

BLU_T1_ARGL = arifle_Mk20_GL_plain_F [30] // MK20C 5.56 GL
BLU_T1_ARGL_MAG = 30Rnd_556x45_Stanag // MK20C

BLU_T2_ARGL = arifle_MX_GL_F [40] // MXC GL 6.65 GL
BLU_T2_ARGL_MAG = 30Rnd_65x39_caseless_mag // MXC 6.65

BLU_T3_AR = srifle_DMR_03_tan_F [60] // MK-1 EMR Tan 7.62
BLU_T3_AR_MAG = 20Rnd_762x51_Mag // MK-1 EMR

BLU_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
BLU_T2_OPTIC = optic_Hamr [40] // 4x Optic
BLU_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic
BLU_T4_OPTIC = optic_KHS_old [60] // Best optic available but costly

[BLU RIFLEMEN ROLE] --
BLU_T1_SMG = SMG_01_F [20] // Vermin 45acp
BLU_T1_SMG_MAG = 30Rnd_45ACP_Mag_SMG_01 // Vermin 45acp

BLU_T1_AR = arifle_Mk20C_plain_F [30] // MK20C 5.56
BLU_T1_AR_MAG = 30Rnd_556x45_Stanag // MK20C 5.56

BLU_T2_AR = arifle_MXC_F [40] // MXC 6.65
BLU_T2_AR_MAG = 30Rnd_65x39_caseless_mag // MXC 6.65

BLU_T3_AR = srifle_DMR_03_tan_F [60] // MK-1 EMR Black 7.62
BLU_T3_AR_MAG = 20Rnd_762x51_Mag // MK-1 EMR

BLU_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
BLU_T2_OPTIC = optic_Hamr [40] // 4x Optic
BLU_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic

[BLU MARKSMEN ROLE] --
BLU_T1_SMG = SMG_01_F [30] // Vermin 45acp
BLU_T1_SMG_MAG = 30Rnd_45ACP_Mag_SMG_01 // Vermin 45acp

BLU_T1_DMR = srifle_DMR_05_tan [40] // Cyrus Tan 9.3mm
BLU_T1_DMR_MAG = 10Rnd_93x64_DMR_05_Mag // Cyrus

BLU_T2_DMR = srifle_DMR_02_sniper_F [50] // MAR 10 TAN .338
BLU_T2_DMR_MAG = 10Rnd_338_Mag // MAR 10 .338

BLU_T3_DMR = srifle_LRR_F [60] // m320 LRR Black .408
BLU_T3_DMR_MAG = 7Rnd_408_Mag // M320 .408

BLU_T4_OPTIC = optic_KHS_old [60] // Only Optic Available for Marksmen

[BLU SUPPORT ROLE] --
BLU_T1_SMG = SMG_01_F [30] // Vermin 45acp
BLU_T1_SMG_MAG = 30Rnd_45ACP_Mag_SMG_01 // Vermin 45acp

BLU_T1_LMG = LMG_Mk200_F [40] // Mk200
BLU_T1_LMG_MAG = 200Rnd_65x39_cased_Box // Zafir

BLU_T2_LMG = MMG_01_tan_F [50] // Navid Tan
BLU_T2_LMG_MAG = 150Rnd_93x64_Mag // Navid

BLU_T3_LMG = MMG_02_tan_F [60] // MMG Camo MTP
BLU_T3_LMG_MAG = 130Rnd_338_Mag // MMG

BLU_T1_OPTIC = optic_Aco [30] // CC Optic With Holo Sight
BLU_T2_OPTIC = optic_Hamr [40] // 4x Optic
BLU_T3_OPTIC = optic_MRCO [50] // Best Mid Range Optic
