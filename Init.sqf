// <Your mission name here> by <your name here>
// Version = <the date here>
// Tested with ArmA 3 <version number>
disableserialization;

enableSaving [false, false];

titleText ["Welcome To ORMP Competitve A3 FireFight", "BLACK FADED", 0.2]; // Fade To Black Intro

// #include "scripts\LW\BorderGuard\BorderGuard.sqf"
// systemChat "BG INIT SUCCESS";
// sleep 1;

#include "scripts\ff_engine\ff_core.sqf"
systemChat "FF INIT SUCCESS";
sleep 1;

systemChat "INIT SUCCESS";
sleep 1;

[] call ff_main;

// This will fade in from black, to hide jarring actions at mission start, this is optional and you can change the value
// Some functions may not continue running properly after loading a saved game, do not delete this line
// All clients stop executing here, do not delete this line
// if (!isServer) exitWith {};

// Execution stops until the mission begins (past briefing), do not delete this line
sleep 5;

// Enter the mission code here
